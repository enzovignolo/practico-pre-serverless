const express = require('express');
const ErroController = require('./controllers/error.controllers');
const routes = require('./routes');
const app = express();

//TODO: apply middlewares
app.use(express.json())
app.use(routes())
app.use('*', (req, res, next) => {
    return res.status(404).json({ message: 'not found' })
})
app.use((err, req, res, next) => {
    let error = ErroController(err)

    return res.status(error.statusCode).json({ message: error.message })
})
module.exports = app;