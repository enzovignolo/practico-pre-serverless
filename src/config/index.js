const dotenv = require('dotenv');
const { Joi } = require('celebrate');
const envFile = dotenv.config({ path: `${process.cwd()}/.env.${process.env.NODE_ENV === 'production' ? 'prod' : 'dev'}` });


const configSchema = Joi.object({
    PORT: Joi.number().required(),
    DB_URI: Joi.string().required(),
    JWT_SECRET: Joi.string().required()
});
const config = {
    PORT: process.env.PORT,
    DB_URI: process.env.DB_URI,
    JWT_SECRET: process.env.JWT_SECRET
}


Joi.assert(config, configSchema, {
    errors: {

    },
});

module.exports = config