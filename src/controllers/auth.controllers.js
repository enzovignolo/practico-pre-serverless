const ErrorCreator = require("../helpers/ErrorCreator");
const { authServices } = require("../services/services");

const AuthControllers = {
    async login(req, res, next) {
        try {
            const userType = (req.baseUrl.replace('/', ''));
            if (!['users', 'stores'].includes(userType)) throw new ErrorCreator('Route not found', 404);
            const { email, password } = req.body;
            const token = await authServices.login({ email, password }, userType)
            return res.status(200).json({ token });
        } catch (err) {
            return next(err);
        }
    }
}


module.exports = AuthControllers;