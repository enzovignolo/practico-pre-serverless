const ErroController = (err) => {
    let error = {
        statusCode: err.statusCode || 500,
        message: err.message || 'INTERNAL SERVER ERROR'
    }
    if (process.env.NODE_ENV === 'development') {
        console.log('Internal error', err, Object.keys(err));
    }
    if (err.code === 11000) {
        error.statusCode = 400;
        error.message = `${JSON.stringify(err.keyValue)} duplicated`
    }
    if (err.message === 'jwt malformed') {
        error.statusCode = 401;
        error.message = 'Unauthenticated'
    }
    return error;
}

module.exports = ErroController;