const { storeServices } = require("../services/services")


const StoreControllers = {

    async create(req, res, next) {
        try {
            const newstore = await storeServices.create(req.body)
            return res.status(201).json(newstore)
        } catch (err) {
            return next(err);
        }
    },

    async getAll(req, res, next) {
        try {
            const { offset, limit } = req.query ? req.query : {};
            const stores = await storeServices.getAll({ offset, limit });

            return res.status(200).json(stores);
        } catch (err) {
            return next(err);
        }
    },
    async getOne(req, res, next) {
        try {
            const store = await storeServices.getOne(req.params.id);
            return res.status(200).json(store)
        } catch (err) {
            return next(err);
        }
    },

    async updateOne(req, res, next) {
        try {
            const updateData = req.body;
            const { id } = req.params;
            const store = await storeServices.updateOne(id, updateData)
            console.log(store);
            return res.status(200).json({ ...store, ...updateData });
        } catch (err) {
            return next(err)
        }
    },
    async deleteOne(req, res, next) {
        try {
            const { id } = req.params
            await storeServices.deleteOne(id)
            return res.status(204).json();
        } catch (err) {

            return next(err)
        }
    }
}

module.exports = StoreControllers;