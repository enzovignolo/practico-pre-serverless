const { userServices } = require("../services/services")

const UserControllers = {

    async create(req, res, next) {
        try {
            const newUser = await userServices.create(req.body)
            return res.status(201).json(newUser)
        } catch (err) {
            next(err);
        }
    },

    async getAll(req, res, next) {
        try {
            const { offset, limit } = req.query ? req.query : {};
            const users = await userServices.getAll({ offset, limit });

            return res.status(200).json(users);
        } catch (err) {
            next(err);
        }
    },
    async getOne(req, res, next) {
        try {
            const user = await userServices.getOne(req.params.id);
            return res.status(200).json(user)
        } catch (err) {
            return next(err);
        }
    },

    async updateOne(req, res, next) {
        try {
            const updateData = req.body;
            const { id } = req.params;
            const user = await userServices.updateOne(id, updateData)
            return res.status(200).json({ ...user, ...updateData });
        } catch (err) {
            return next(err)
        }
    },
    async deleteOne(req, res, next) {
        try {
            const { id } = req.params
            await userServices.deleteOne(id)
            return res.status(204).json();
        } catch (err) {

            return next(err)
        }
    }
}


module.exports = UserControllers