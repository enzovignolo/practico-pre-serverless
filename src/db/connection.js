const mongoose = require('mongoose');
const config = require('../config');

const dbConnect = async () => {
    try {
        await mongoose.connect(config.DB_URI)
    } catch (err) {
        return err
    }
}
module.exports = {

    dbConnect

}