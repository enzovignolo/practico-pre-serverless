import Store from "./store.model";
import User from "./user.model";

export default {
    User: User,
    Store: Store
}