const { Schema, Types, model } = require('mongoose')
const StoreSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },

})

const Store = model('store', StoreSchema)

module.exports = Store;