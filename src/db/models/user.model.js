const { Schema, Types, model } = require('mongoose')
const UserSchema = new Schema({
    firstname: {
        type: String,
        required: true
    },
    lastname: {
        type: String,
        required: true
    },
    creditCards: [{

        limit: {
            type: Number,
            required: true
        },
        isActive: {
            type: Boolean,
            requried: true
        },
        hasDebt: {
            type: Boolean,
            required: true
        },
        pan: {
            type: String,
            required: true
        }
    }],
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },

})

const User = model('user', UserSchema)

module.exports = User;