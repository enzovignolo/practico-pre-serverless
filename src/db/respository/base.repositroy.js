class BaseRepository {
    constructor(Model) {

        this.model = Model;
    };
    async findAll() {
        let selectObj = {};
        if (['user', 'store'].includes(this.model.modelName)) {
            selectObj = {
                password: false
            }
        }
        return await this.model.find({}, selectObj);
    }
    async findById(id) {
        let selectObj = {};
        if (this.model.modelName === 'user') {
            selectObj = {
                password: false
            }
        }
        return await this.model.findById(id, selectObj)
    }
    async create(user) {

        const newUser = await this.model.create(user);
        return newUser;
    }
    async deleteOne(id) {
        return await this.model.findByIdAndDelete(id)
    }
    async updateOne(id, updateData) {
        let selectObj = {};
        if (['user', 'store'].includes(this.model.modelName)) {
            selectObj = {
                password: false
            }
        }
        return await this.model.findByIdAndUpdate(id, updateData, { lean: true, new: true, select: selectObj })
    }
}

module.exports = BaseRepository;