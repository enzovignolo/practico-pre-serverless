const Store = require("../models/store.model");
const BaseRepository = require("./base.repositroy");

class StoreRepository extends BaseRepository {
    constructor() {
        super(Store);
    }
    async findByEmail(email) {
        try {
            console.log(email);
            const store = await Store.findOne({ email });
            if (!store) throw new ErrorCreator('Not found', 404);
            return store;
        } catch (err) {
            throw err;
        }
    }


}

module.exports = StoreRepository;