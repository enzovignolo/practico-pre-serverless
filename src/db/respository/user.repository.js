const ErrorCreator = require("../../helpers/ErrorCreator");
const User = require("../models/user.model");
const BaseRepository = require("./base.repositroy");

class UserRepository extends BaseRepository {
    constructor() {
        super(User);
    }
    async findByEmail(email) {
        try {
            const user = await User.findOne({ email });
            if (!user) throw new ErrorCreator('Not found', 404);
            return user;
        } catch (err) {
            throw err;
        }
    }

}

module.exports = UserRepository;