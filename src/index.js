const app = require("./app.js");
const config = require("./config/index.js");
const { dbConnect } = require("./db/connection.js");
const { PORT } = config;
app.listen(PORT, async () => {
    console.log(`[OK] API Running on PORT ${PORT}`);
    await dbConnect()
    console.log(`[DB] Connected`);
})

