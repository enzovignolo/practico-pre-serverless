const jwt = require('jsonwebtoken');
const config = require('../config');
const ErrorCreator = require('../helpers/ErrorCreator');
const { authServices } = require('../services/services');
const AuthMiddlewares = {
    async isLogged(req, res, next) {
        try {
            if (!req.headers.authorization) throw new ErrorCreator('Unauthenticated', 401);
            const token = req.headers.authorization.split('Bearer ')[1];

            const accessInfo = await jwt.verify(token, config.JWT_SECRET);

            req.user = accessInfo;
            return next()
        } catch (err) {
            return next(err)
        }
    },
    /**
     * Middleware to check if user is trying to access self resource
     * @param {*} req 
     * @param {*} res 
     * @param {*} next 
     */
    async isSelf(req, res, next) {
        try {
            console.log('aa', req.user);
            await authServices.verifySelf(req.user, req.params.id);
            return next()
        } catch (err) {
            return next(err)
        }
    }
}

module.exports = AuthMiddlewares;