const { Router } = require("express");
const storeRoutes = require("./store.routes");
const userRoutes = require("./user.routes")

module.exports = () => {
    const app = Router();
    app.use('/users', userRoutes);
    app.use('/stores', storeRoutes);

    return app;
}