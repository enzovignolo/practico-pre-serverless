const { Router } = require("express");
const AuthControllers = require("../controllers/auth.controllers");
const StoreControllers = require("../controllers/store.controllers");
const AuthMiddlewares = require("../middlewares/auth.middlewares");


const storeRoutes = Router();
storeRoutes.route('/').get(StoreControllers.getAll).post(StoreControllers.create);
storeRoutes.route('/login').post(AuthControllers.login);
storeRoutes.route('/:id').get(StoreControllers.getOne).patch(AuthMiddlewares.isLogged, AuthMiddlewares.isSelf, StoreControllers.updateOne).delete(AuthMiddlewares.isLogged, AuthMiddlewares.isSelf, StoreControllers.deleteOne);

module.exports = storeRoutes;
