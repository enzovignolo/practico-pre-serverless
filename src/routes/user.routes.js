const { Router } = require("express");
const AuthControllers = require("../controllers/auth.controllers");
const UserControllers = require("../controllers/user.controllers");
const AuthMiddlewares = require("../middlewares/auth.middlewares");


const userRoutes = Router();
userRoutes.route('/').get(AuthMiddlewares.isLogged, UserControllers.getAll).post(UserControllers.create);
userRoutes.post('/login', AuthControllers.login)
userRoutes.route('/:id').get(UserControllers.getOne).patch(AuthMiddlewares.isLogged, AuthMiddlewares.isSelf, UserControllers.updateOne).delete(AuthMiddlewares.isLogged, AuthMiddlewares.isSelf, UserControllers.deleteOne);

module.exports = userRoutes;
