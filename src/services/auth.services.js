const { compare } = require('bcrypt');
const ErrorCreator = require('../helpers/ErrorCreator');
const jwt = require('jsonwebtoken');
const config = require('../config');
const AuthServices = (UserRepository, StoreRepository) => {
    const userRepository = new UserRepository();
    const storeRepository = new StoreRepository();
    return {
        async login(loginInfo, userType) {
            try {
                let password;
                if (userType === 'users') {
                    const user = await userRepository.findByEmail(loginInfo.email);
                    password = user.password
                } else {
                    const store = await storeRepository.findByEmail(loginInfo.email);
                    password = store.password

                }
                if (!await compare(loginInfo.password, password)) throw new ErrorCreator('Wrong credentials', 401);
                const token = jwt.sign({ email: loginInfo.email, userType }, config.JWT_SECRET);
                return token;

            } catch (err) {
                throw err;
            }
        },
        async verifySelf({ email, userType }, idToAccess) {
            try {
                let dataToAccess;
                if (userType === 'users') {

                    dataToAccess = await userRepository.findById(idToAccess);
                } else {
                    dataToAccess = await storeRepository.findById(idToAccess);

                }
                if (!dataToAccess || (email != dataToAccess.email)) throw new ErrorCreator('This user can not perform this action', 403);
                return;
            } catch (err) {
                throw err
            }
        }
    };
};

module.exports = AuthServices;