const ErrorCreator = require("../helpers/ErrorCreator");

const factoryCRUD = (repository) => {
    return {
        create(user) {
            const newData = repository.create(user);
            return newData;
        },

        getAll() {

            return repository.findAll();
        },
        async getOne(id) {
            const data = await repository.findById(id);
            if (!data) throw new ErrorCreator('Not found', 404)
            return data
        },
        async updateOne(id, dataUpdate) {
            const updatedData = await repository.updateOne(id, dataUpdate);
            if (!updatedData) throw new ErrorCreator('Not found', 404);
            console.log('object', updatedData);
            return
        },

        async deleteOne(id) {
            return await repository.deleteOne(id)
        },

    }
}

module.exports = factoryCRUD;