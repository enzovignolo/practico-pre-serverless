const StoreRepository = require("../db/respository/store.repository");
const UserRepository = require("../db/respository/user.repository");
const AuthServices = require("./auth.services");
const StoreServices = require("./store.services");
const UserServices = require("./user.services");

module.exports = {
    userServices: UserServices(UserRepository),
    storeServices: StoreServices(StoreRepository),
    authServices: AuthServices(UserRepository, StoreRepository)
}