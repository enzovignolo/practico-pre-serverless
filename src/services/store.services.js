const { hash } = require("bcrypt");
const ErrorCreator = require("../helpers/ErrorCreator");
const factoryCRUD = require("./factoryCRUD.services");

const StoreServices = (StoreRepository) => {
    const storeRepository = new StoreRepository();
    return {
        ...factoryCRUD(storeRepository),
        async create(store) {
            try {

                const encryptedPass = await hash(store.password, 12);
                const newData = storeRepository.create({ ...store, password: encryptedPass });
                return newData;
            } catch (err) {
                throw err;
            }
        },
        async updateOne(id, storeUpdate) {
            let encryptedPass;
            if (storeUpdate.password) {

                encryptedPass = await hash(storeUpdate.password, 12);
            }
            const updatedStore = await storeRepository.updateOne(id, { ...storeUpdate, password: encryptedPass });
            if (!updatedStore) throw new ErrorCreator('Not found', 404)
            return updatedStore;
        },

    }
};
module.exports = StoreServices;