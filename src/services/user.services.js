const { hash } = require("bcrypt");
const ErrorCreator = require("../helpers/ErrorCreator");
const factoryCRUD = require("./factoryCRUD.services");

const UserServices = (UserRepository) => {
    const userRepository = new UserRepository();
    return {
        ...factoryCRUD(userRepository),
        async create(user) {
            try {

                const encryptedPass = await hash(user.password, 12);
                const newData = userRepository.create({ ...user, password: encryptedPass });
                return newData;
            } catch (err) {
                throw err;
            }
        },
        async updateOne(id, userUpdate) {
            let encryptedPass;
            if (userUpdate.password) {

                encryptedPass = await hash(userUpdate.password, 12);
            }
            const updatedUser = await userRepository.updateOne(id, { ...userUpdate, password: encryptedPass });
            if (!updatedUser) throw new ErrorCreator('Not found', 404);
            return updatedUser;
        },

    }
};
module.exports = UserServices;