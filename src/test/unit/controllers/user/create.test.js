
const { create } = require("../../../../controllers/user.controllers");
const { userServices } = require("../../../../services/services");


jest.mock('../../../../services/services');
const dummyUser = {
    firstname: 'DummyFirst',
    lastname: 'DummyLast',
    creditCards: [{

        limit: 123,
        isActive: true,
        hasDebt: false,
        pan: '************2558'
    }],
    email: 'dummy@mail.com',
    password: '12345678',
};
describe('When create is called with a valid user', () => {
    beforeEach(() => {
        userServices.create.mockClear();
    });
    it('Should return HTTP status 201 on success', () => {

        const mReq = { body: dummyUser };
        const mRes = {
            status: jest.fn().mockReturnThis(),
            json: jest.fn()
        }

        userServices.create.mockReturnValue(dummyUser)
        const mNext = () => { }
        create(mReq, mRes, mNext);
        expect(mRes.status).toBeCalledWith(201);
        expect(mRes.json).toBeCalledWith(dummyUser)
    });
    it('Should call userCreate service once with user input', () => {

        const mReq = { body: dummyUser };
        const mRes = {
            status: jest.fn().mockReturnThis(),
            json: jest.fn()
        }
        const mNext = () => { }
        create(mReq, mRes, mNext);
        expect(userServices.create).toHaveBeenCalledTimes(1)
        expect(userServices.create).toHaveBeenNthCalledWith(1, dummyUser)

    });
})

