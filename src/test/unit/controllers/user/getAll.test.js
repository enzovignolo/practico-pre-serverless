
const { getAll } = require("../../../../controllers/user.controllers");
const { userServices } = require("../../../../services/services");


jest.mock('../../../../services/services');
const dummyUsers = [{
    firstname: 'DummyFirst',
    lastname: 'DummyLast',
    creditCards: [{

        limit: 123,
        isActive: true,
        hasDebt: false,
        pan: '************2558'
    }],
    email: 'dummy@mail.com',
    password: '12345678',
}, {
    firstname: 'DummyFirst1',
    lastname: 'DummyLast1',
    creditCards: [{

        limit: 223,
        isActive: true,
        hasDebt: false,
        pan: '************2559'
    }],
    email: 'dummy1@mail.com',
    password: '123456781',
}, {
    firstname: 'DummyFirst3',
    lastname: 'DummyLast3',
    creditCards: [{

        limit: 300,
        isActive: true,
        hasDebt: false,
        pan: '************2520'
    }],
    email: 'dummy3@mail.com',
    password: '123456783',
}

]
describe('When call getAll with no arguments', () => {
    beforeEach(() => {
        userServices.getAll.mockClear()
    })
    it('Should call status function with 200 and json function with all Users', () => {
        const mReq = {};
        const mRes = {
            status: jest.fn().mockReturnThis(),
            json: jest.fn()
        }
        const mNext = () => { };
        userServices.getAll.mockReturnValue(dummyUsers)
        getAll(mReq, mRes, mNext);
        expect(mRes.status).toHaveBeenNthCalledWith(1, 200);
        expect(mRes.json).toHaveBeenNthCalledWith(1, dummyUsers);


    })
    it('Should call getAll service once with no arguments', () => {
        const mReq = {};
        const mRes = {
            status: jest.fn().mockReturnThis(),
            json: jest.fn()
        }
        const mNext = () => { };
        getAll(mReq, mRes, mNext);
        expect(userServices.getAll).toHaveBeenCalledTimes(1)
    })

})

describe('When getAll is called with query string for pagination', () => {
    beforeEach(() => {
        userServices.getAll.mockClear()
    })
    it('Should call getAll service with pagination options', () => {
        const mReq = {
            query: {
                offset: 2,
                limit: 10

            }
        };
        const mRes = {
            status: jest.fn().mockReturnThis(),
            json: jest.fn()
        }
        const mNext = () => { };
        getAll(mReq, mRes, mNext);
        expect(userServices.getAll).toHaveBeenNthCalledWith(1, { offset: mReq.query.offset, limit: mReq.query.limit })

    })
})