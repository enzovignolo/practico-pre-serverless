const { getOne } = require('../../../../controllers/user.controllers');
const { userServices } = require('../../../../services/services');

jest.mock('../../../../services/services');
describe('When call getOne with a valid id ', () => {
    const dummyUsers = [{
        _id: '1',
        firstname: 'DummyFirst',
        lastname: 'DummyLast',
        creditCards: [{

            limit: 123,
            isActive: true,
            hasDebt: false,
            pan: '************2558'
        }],
        email: 'dummy@mail.com',
        password: '12345678',
    }, {
        _id: '2',
        firstname: 'DummyFirst1',
        lastname: 'DummyLast1',
        creditCards: [{

            limit: 223,
            isActive: true,
            hasDebt: false,
            pan: '************2559'
        }],
        email: 'dummy1@mail.com',
        password: '123456781',
    }, {
        _id: '3',
        firstname: 'DummyFirst3',
        lastname: 'DummyLast3',
        creditCards: [{

            limit: 300,
            isActive: true,
            hasDebt: false,
            pan: '************2520'
        }],
        email: 'dummy3@mail.com',
        password: '123456783',
    }

    ];
    beforeEach(() => {
        userServices.getOne.mockClear()
    })
    it('Should call status with 200 and json with User', () => {
        const mReq = { params: { id: dummyUsers[0]._id } };
        const mRes = { status: jest.fn().mockReturnThis(), json: jest.fn() };
        const mNext = () => { };
        userServices.getOne.mockReturnValue(dummyUsers[0]);
        getOne(mReq, mRes, mNext);
        expect(mRes.status).toHaveBeenNthCalledWith(1, 200);
        expect(mRes.json).toHaveBeenNthCalledWith(1, dummyUsers[0]);

    })
    it('Should call getOne from user service with id param', () => {
        const mReq = { params: { id: dummyUsers[0]._id } };
        const mRes = { status: jest.fn().mockReturnThis(), json: jest.fn() };
        const mNext = () => { };
        userServices.getOne.mockReturnValue(dummyUsers[0]);
        getOne(mReq, mRes, mNext);
        expect(userServices.getOne).toHaveBeenCalledTimes(1)
        expect(userServices.getOne).toHaveBeenNthCalledWith(1, mReq.params.id)
    })
})