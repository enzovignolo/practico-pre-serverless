const User = require("../../../db/models/user.model");

describe('Test for model validations', () => {
    const dummyValidUser = {


        firstname: 'DummyFirst',
        lastname: 'DummyLast',
        creditCards: [{

            limit: 123,
            isActive: true,
            hasDebt: false,
            pan: '************2558'
        }],
        email: 'dummy@mail.com',
        password: '12345678',

    };
    it('Shoul not throw any validation error when called with valid user', async () => {
        let error;
        try {
            const user = new User(dummyValidUser);
            await user.validate()
        } catch (err) {
            error = err;
        }

        expect(error).not.toBeDefined()
    })
    it('Should not let insert user with no firstname', async () => {
        const { firstname, ...invalidDummyUser } = dummyValidUser;
        let error;
        try {
            const user = new User(invalidDummyUser);
            await user.validate()
        } catch (err) {
            error = err;
        }

        expect(error.errors.firstname).toBeDefined();
        expect(error.name).toBe('ValidationError');

    })
    it('Should not let insert user with no lastname', async () => {
        const { lastname, ...invalidDummyUser } = dummyValidUser;
        let error;
        try {
            const user = new User(invalidDummyUser);
            await user.validate()
        } catch (err) {
            error = err;
        }

        expect(error.errors.lastname).toBeDefined();
        expect(error.name).toBe('ValidationError');

    })
    it('Should not let insert user with no email', async () => {
        const { email, ...invalidDummyUser } = dummyValidUser;
        let error;
        try {
            const user = new User(invalidDummyUser);
            await user.validate()
        } catch (err) {
            error = err;
        }
        expect(error.errors.email).toBeDefined();
        expect(error.name).toBe('ValidationError');

    })
    it('Should not let insert user with no password', async () => {
        const { password, ...invalidDummyUser } = dummyValidUser;
        let error;
        try {
            const user = new User(invalidDummyUser);
            await user.validate()
        } catch (err) {
            error = err;
        }

        expect(error.errors.password).toBeDefined()
        expect(error.name).toBe('ValidationError');
    })
    it('Should allow create a User instance with _id property', async () => {
        const user = new User(dummyValidUser)

        expect(user).toHaveProperty('_id')

    })
})