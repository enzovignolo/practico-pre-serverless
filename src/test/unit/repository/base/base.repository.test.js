const BaseRepository = require("../../../../db/respository/base.repositroy")

describe('When called with a Model', () => {
    const MockModel = {};
    const MockRepositroy = new BaseRepository(MockModel)
    it('Should return an instance with create function', () => {
        expect(MockRepositroy).toHaveProperty('create')
    })
    it('Should return an instance with findAll function', () => {
        expect(MockRepositroy).toHaveProperty('findAll')
    })
    it('Should return an instance with findById function', () => {
        expect(MockRepositroy).toHaveProperty('findById')
    })
    it('Should return an instance with updateOne function', () => {
        expect(MockRepositroy).toHaveProperty('updateOne')
    })
    it('Should return an instance with deleteOne function', () => {
        expect(MockRepositroy).toHaveProperty('deleteOne')
    })

})