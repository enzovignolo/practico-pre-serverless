const User = require("../../../../db/models/user.model");
const UserRepository = require("../../../../db/respository/user.repository")

const userRepository = new UserRepository();
jest.mock("../../../../db/models/user.model");
const createUser = jest.spyOn(User, 'create').mockImplementation((user) => {
    return { _id: '123123', ...user }
})
const dummyUser = {

    firstname: 'DummyFirst',
    lastname: 'DummyLast',
    creditCards: [{

        limit: 123,
        isActive: true,
        hasDebt: false,
        pan: '************2558'
    }],
    email: 'dummy@mail.com',
    password: '12345678',
};
describe('When call create repository with user', () => {
    beforeEach(() => {
        createUser.mockClear();
    })
    it('Should return the user created', () => {
        const user = userRepository.create(dummyUser);
        const expectedResult = { _id: '123123', ...dummyUser };
        expect(user).resolves.toStrictEqual(expectedResult)
    })
    it('Should call create once on model with user', () => {

        const user = userRepository.create(dummyUser);
        expect(createUser).toHaveBeenCalledTimes(1);
        expect(createUser).toHaveBeenNthCalledWith(1, dummyUser);

    })

})