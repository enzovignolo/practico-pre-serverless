const User = require("../../../../db/models/user.model");
const UserRepository = require("../../../../db/respository/user.repository")

const userRepository = new UserRepository();

const dummyUsers = [{
    _id: '123123',
    firstname: 'DummyFirst',
    lastname: 'DummyLast',
    creditCards: [{

        limit: 123,
        isActive: true,
        hasDebt: false,
        pan: '************2558'
    }],
    email: 'dummy@mail.com',
    password: '12345678',
}, {
    _id: '123124',
    firstname: 'DummyFirst1',
    lastname: 'DummyLast1',
    creditCards: [{

        limit: 223,
        isActive: true,
        hasDebt: false,
        pan: '************2559'
    }],
    email: 'dummy1@mail.com',
    password: '123456781',
}, {
    _id: '123125',
    firstname: 'DummyFirst3',
    lastname: 'DummyLast3',
    creditCards: [{

        limit: 300,
        isActive: true,
        hasDebt: false,
        pan: '************2520'
    }],
    email: 'dummy3@mail.com',
    password: '123456783',
}

];
jest.mock("../../../../db/models/user.model")
const findByIdAndDelete = jest.spyOn(User, 'findByIdAndDelete').mockImplementation((id) => {
    return true;
})
describe('When deleteOne is called with no arguments', () => {
    beforeEach(() => {
        findByIdAndDelete.mockClear();
    })
    it('Should return true', () => {
        const id = dummyUsers[0]._id
        const wasDeleted = userRepository.deleteOne(id);
        const expectedResult = true;
        expect(wasDeleted).resolves.toStrictEqual(expectedResult);
    })
    it('Should call findByIdAndDelete once with id and data passed', () => {
        const id = dummyUsers[0]._id

        const wasDeleted = userRepository.deleteOne(id);
        expect(findByIdAndDelete).toHaveBeenCalledTimes(1);
        expect(findByIdAndDelete).toHaveBeenNthCalledWith(1, id)
    })
})