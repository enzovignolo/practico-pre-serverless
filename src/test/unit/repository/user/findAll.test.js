const User = require("../../../../db/models/user.model");
const UserRepository = require("../../../../db/respository/user.repository")

const userRepository = new UserRepository();

const dummyUsers = [{
    firstname: 'DummyFirst',
    lastname: 'DummyLast',
    creditCards: [{

        limit: 123,
        isActive: true,
        hasDebt: false,
        pan: '************2558'
    }],
    email: 'dummy@mail.com',
    password: '12345678',
}, {
    firstname: 'DummyFirst1',
    lastname: 'DummyLast1',
    creditCards: [{

        limit: 223,
        isActive: true,
        hasDebt: false,
        pan: '************2559'
    }],
    email: 'dummy1@mail.com',
    password: '123456781',
}, {
    firstname: 'DummyFirst3',
    lastname: 'DummyLast3',
    creditCards: [{

        limit: 300,
        isActive: true,
        hasDebt: false,
        pan: '************2520'
    }],
    email: 'dummy3@mail.com',
    password: '123456783',
}

];
jest.mock("../../../../db/models/user.model")
const find = jest.spyOn(User, 'find').mockImplementation(() => {
    return dummyUsers
})
describe('When findAll is called with no arguments', () => {
    beforeEach(() => {
        find.mockClear();
    })
    it('Should return all users', () => {
        const users = userRepository.findAll();
        const expectedResult = dummyUsers;
        expect(users).resolves.toStrictEqual(expectedResult);
    })
    it('Should call find on model once with no argument', () => {
        const users = userRepository.findAll();
        expect(find).toHaveBeenCalledTimes(1);
        expect(find).toHaveBeenNthCalledWith(1)
    })
})