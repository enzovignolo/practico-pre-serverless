const User = require("../../../../db/models/user.model");
const UserRepository = require("../../../../db/respository/user.repository")

const userRepository = new UserRepository();

const dummyUsers = [{
    _id: '123123',
    firstname: 'DummyFirst',
    lastname: 'DummyLast',
    creditCards: [{

        limit: 123,
        isActive: true,
        hasDebt: false,
        pan: '************2558'
    }],
    email: 'dummy@mail.com',
    password: '12345678',
}, {
    _id: '123124',
    firstname: 'DummyFirst1',
    lastname: 'DummyLast1',
    creditCards: [{

        limit: 223,
        isActive: true,
        hasDebt: false,
        pan: '************2559'
    }],
    email: 'dummy1@mail.com',
    password: '123456781',
}, {
    _id: '123125',
    firstname: 'DummyFirst3',
    lastname: 'DummyLast3',
    creditCards: [{

        limit: 300,
        isActive: true,
        hasDebt: false,
        pan: '************2520'
    }],
    email: 'dummy3@mail.com',
    password: '123456783',
}

];
jest.mock("../../../../db/models/user.model")
const findByIdMock = jest.spyOn(User, 'findById').mockImplementation((id) => {
    return dummyUsers[0];
})
describe('When findOneById is called with no arguments', () => {
    beforeEach(() => {
        findByIdMock.mockClear();
    })
    it('Should return one user with id passed', () => {
        const id = dummyUsers[0]._id
        const user = userRepository.findById(id);
        const expectedResult = dummyUsers[0];

        expect(user).resolves.toStrictEqual(expectedResult);
    })
    it('Should call findById once with id passed', () => {
        const id = dummyUsers[0]._id

        const users = userRepository.findById(id);
        expect(findByIdMock).toHaveBeenCalledTimes(1);
        expect(findByIdMock).toHaveBeenNthCalledWith(1, id)
    })
})