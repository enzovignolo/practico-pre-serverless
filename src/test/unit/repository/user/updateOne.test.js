const User = require("../../../../db/models/user.model");
const UserRepository = require("../../../../db/respository/user.repository")

const userRepository = new UserRepository();

const dummyUsers = [{
    _id: '123123',
    firstname: 'DummyFirst',
    lastname: 'DummyLast',
    creditCards: [{

        limit: 123,
        isActive: true,
        hasDebt: false,
        pan: '************2558'
    }],
    email: 'dummy@mail.com',
    password: '12345678',
}, {
    _id: '123124',
    firstname: 'DummyFirst1',
    lastname: 'DummyLast1',
    creditCards: [{

        limit: 223,
        isActive: true,
        hasDebt: false,
        pan: '************2559'
    }],
    email: 'dummy1@mail.com',
    password: '123456781',
}, {
    _id: '123125',
    firstname: 'DummyFirst3',
    lastname: 'DummyLast3',
    creditCards: [{

        limit: 300,
        isActive: true,
        hasDebt: false,
        pan: '************2520'
    }],
    email: 'dummy3@mail.com',
    password: '123456783',
}

];
jest.mock("../../../../db/models/user.model")
const findByIdAndUpdate = jest.spyOn(User, 'findByIdAndUpdate').mockImplementation((id, updateData) => {
    return { ...dummyUsers[0], ...updateData };
})
describe('When updateOne is called with no arguments', () => {
    beforeEach(() => {
        findByIdAndUpdate.mockClear();
    })
    it('Should return user updated', () => {
        const id = dummyUsers[0]._id
        const updateData = { firstname: 'updateData' }
        const user = userRepository.updateOne(id, updateData);
        const expectedResult = { ...dummyUsers[0], ...updateData };

        expect(user).resolves.toStrictEqual(expectedResult);
    })
    it('Should call findByIdAndUpdate once with id and data passed', () => {
        const id = dummyUsers[0]._id
        const updateData = { firstname: 'updateData' }

        const users = userRepository.updateOne(id, updateData);
        expect(findByIdAndUpdate).toHaveBeenCalledTimes(1);
        expect(findByIdAndUpdate).toHaveBeenNthCalledWith(1, id, updateData)
    })
})