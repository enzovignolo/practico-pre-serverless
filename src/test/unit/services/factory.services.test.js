const factoryCRUD = require("../../../services/factoryCRUD.services")

describe('When factoryCRUD services called with a repository', () => {
    const mockCRUDServices = factoryCRUD({});
    it('Should return a an object with a create function', () => {
        expect(mockCRUDServices).toHaveProperty('create')
    })
    it('Should return a an object with a getOne function', () => {
        expect(mockCRUDServices).toHaveProperty('getOne')
    })
    it('Should return a an object with a getAll function', () => {
        expect(mockCRUDServices).toHaveProperty('getAll')
    })
    it('Should return a an object with a deleteOne function', () => {
        expect(mockCRUDServices).toHaveProperty('deleteOne')
    })
    it('Should return a an object with a updateOne function', () => {
        expect(mockCRUDServices).toHaveProperty('updateOne')
    })
})