const UserRepository = require('../../../../db/respository/user.repository');
const { userServices } = require('../../../../services/services');
//const { create } = require('../../../../services/user.services')

//Mocking repository create method

jest.mock('../../../../db/respository/user.repository')
const createRepository = jest.spyOn(UserRepository.prototype, 'create').mockImplementation((user) => { return { _id: '1', ...user } })
const { create } = userServices;
describe('When call create with a valid user', () => {
    const dummyUser = {

        firstname: 'DummyFirst',
        lastname: 'DummyLast',
        creditCards: [{

            limit: 123,
            isActive: true,
            hasDebt: false,
            pan: '************2558'
        }],
        email: 'dummy@mail.com',
        password: '12345678',
    };
    beforeEach(() => {
        createRepository.mockClear()
    })
    it('Should return the created user', () => {
        const expectedResult = { _id: '1', ...dummyUser }
        const userCreated = create(dummyUser);


        expect(userCreated).toStrictEqual(expectedResult);
    })
    it('Should call create userRepository method once with user', () => {

        create(dummyUser);
        expect(createRepository).toHaveBeenCalledTimes(1)
    })

})
