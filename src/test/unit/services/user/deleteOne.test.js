const UserRepository = require("../../../../db/respository/user.repository");
const { userServices } = require("../../../../services/services");
const { deleteOne } = userServices;

jest.mock("../../../../db/respository/user.repository")
const deleteOneRepository = jest.spyOn(UserRepository.prototype, 'deleteOne').mockImplementation(() => {
    return;
})

describe('When call getOne services with valid id', () => {

    beforeEach(() => {
        deleteOneRepository.mockClear();
    })
    it('Should return void response', () => {
        const response = deleteOne('1');
        expect(response).toStrictEqual(undefined);
    })
    it('Should call deleteOne once with id ', () => {
        const id = '1';
        const user = deleteOne(id);
        expect(deleteOneRepository).toHaveBeenCalledTimes(1);
        expect(deleteOneRepository).toHaveBeenNthCalledWith(1, id);
    })
})
