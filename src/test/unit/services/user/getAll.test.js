const UserRepository = require("../../../../db/respository/user.repository");
const { userServices } = require("../../../../services/services");
const { getAll } = userServices;

const dummyUsers = [{
    firstname: 'DummyFirst',
    lastname: 'DummyLast',
    creditCards: [{

        limit: 123,
        isActive: true,
        hasDebt: false,
        pan: '************2558'
    }],
    email: 'dummy@mail.com',
    password: '12345678',
}, {
    firstname: 'DummyFirst1',
    lastname: 'DummyLast1',
    creditCards: [{

        limit: 223,
        isActive: true,
        hasDebt: false,
        pan: '************2559'
    }],
    email: 'dummy1@mail.com',
    password: '123456781',
}, {
    firstname: 'DummyFirst3',
    lastname: 'DummyLast3',
    creditCards: [{

        limit: 300,
        isActive: true,
        hasDebt: false,
        pan: '************2520'
    }],
    email: 'dummy3@mail.com',
    password: '123456783',
}

];
jest.mock("../../../../db/respository/user.repository")
const findAll = jest.spyOn(UserRepository.prototype, 'findAll').mockImplementation(() => {
    return dummyUsers;
})
describe('When call getAll services with no arguments', () => {
    beforeEach(() => {
        findAll.mockClear();
    })
    it('Should return array of all users ', () => {
        const users = getAll();
        expect(users).toStrictEqual(dummyUsers);

    })
    it('Should call findAll once with no arguments', () => {
        const users = getAll();
        expect(findAll).toHaveBeenCalledTimes(1);
        expect(findAll).toHaveBeenNthCalledWith(1);
    })
})
