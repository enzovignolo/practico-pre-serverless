const UserRepository = require("../../../../db/respository/user.repository");
const { userServices } = require("../../../../services/services");
const { getOne } = userServices;


const dummyUsers = [{
    _id: '123123',
    firstname: 'DummyFirst',
    lastname: 'DummyLast',
    creditCards: [{

        limit: 123,
        isActive: true,
        hasDebt: false,
        pan: '************2558'
    }],
    email: 'dummy@mail.com',
    password: '12345678',
}, {
    _id: '123124',
    firstname: 'DummyFirst1',
    lastname: 'DummyLast1',
    creditCards: [{

        limit: 223,
        isActive: true,
        hasDebt: false,
        pan: '************2559'
    }],
    email: 'dummy1@mail.com',
    password: '123456781',
}, {
    _id: '123125',
    firstname: 'DummyFirst3',
    lastname: 'DummyLast3',
    creditCards: [{

        limit: 300,
        isActive: true,
        hasDebt: false,
        pan: '************2520'
    }],
    email: 'dummy3@mail.com',
    password: '123456783',
}

];
jest.mock("../../../../db/respository/user.repository")
const findById = jest.spyOn(UserRepository.prototype, 'findById').mockImplementation((id) => {
    return { ...dummyUsers[0], _id: id };
})
describe('When call getOne services with valid id', () => {
    beforeEach(() => {
        findById.mockClear();
    })
    it('Should return user with id ', () => {
        const id = dummyUsers[0]._id;
        const user = getOne(id);
        expect(user).toStrictEqual(dummyUsers[0]);
    })
    it('Should call findOne once with id ', () => {
        const id = dummyUsers[0]._id;
        const user = getOne(id);
        expect(findById).toHaveBeenCalledTimes(1);
        expect(findById).toHaveBeenNthCalledWith(1, id);
    })
})