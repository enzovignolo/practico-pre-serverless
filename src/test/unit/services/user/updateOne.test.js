const UserRepository = require("../../../../db/respository/user.repository");
const { userServices } = require("../../../../services/services");
const { updateOne } = userServices;

const dummyUsers = [{
    firstname: 'DummyFirst',
    lastname: 'DummyLast',
    creditCards: [{

        limit: 123,
        isActive: true,
        hasDebt: false,
        pan: '************2558'
    }],
    email: 'dummy@mail.com',
    password: '12345678',
}, {
    firstname: 'DummyFirst1',
    lastname: 'DummyLast1',
    creditCards: [{

        limit: 223,
        isActive: true,
        hasDebt: false,
        pan: '************2559'
    }],
    email: 'dummy1@mail.com',
    password: '123456781',
}, {
    firstname: 'DummyFirst3',
    lastname: 'DummyLast3',
    creditCards: [{

        limit: 300,
        isActive: true,
        hasDebt: false,
        pan: '************2520'
    }],
    email: 'dummy3@mail.com',
    password: '123456783',
}

];
jest.mock("../../../../db/respository/user.repository")
const updateOneRepository = jest.spyOn(UserRepository.prototype, 'updateOne').mockImplementation((id, updatedData) => {
    return { ...dummyUsers[0], ...updatedData };
})
describe('When call updateOne services with valid id and user data', () => {
    beforeEach(() => {
        updateOneRepository.mockClear();
    })
    it('Should return user with id ', () => {
        const updatedData = { firstname: 'updatedName' }
        const resultExpected = { ...dummyUsers[0], ...updatedData }
        const user = updateOne('1', updatedData);
        expect(user).toStrictEqual(resultExpected);
    })
    it('Should call updateOne once with id ', () => {
        const id = '1';
        const updatedData = { firstname: 'updatedName' }

        const user = updateOne(id, updatedData);
        expect(updateOneRepository).toHaveBeenCalledTimes(1);
        expect(updateOneRepository).toHaveBeenNthCalledWith(1, id, updatedData);
    })
})